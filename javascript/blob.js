//complete circle and line vs touch everything
//circle and everything around circle

var path;
var UNSELECTED_COLOR='green';
var SELECTED_COLOR='pink';
var UNSELECTED_OPACITY=.6;
var SELECTED_OPACITY=1;
var arr=[];
var center=[];
var selected=[];
var radius=50;

function messageSudden(str){
    alert(str);
}
function drawGroup(group){
    var column=Math.ceil(Math.sqrt(group.length));
    for(var i=0;group.length>i;i++){
        pt=new Point((i%column)*radius*2+radius,Math.floor(i/column)*radius*2+radius);
        createNode(pt,group[i]);
    }
}

function createImage(id,pt,node){
    var img=document.createElement("img");
    img.setAttribute('src', friends[id].picture);
    img.setAttribute('id', id);
    var raster = new Raster(id);
    raster.position = pt;
    // As the web is asynchronous, we need to wait for the raster to load
    // before we can perform any operation on its pixels.
    raster.on('load', function() {
        // Downsize the pixel content to 80 pixels wide and 60 pixels high:
        // raster.size = new Size(80, 60);
    });
    var group = new Group([node, raster]);
    group.position = pt;
    group.clipMask = false;
    group.clipped = true;
    group.opacity = UNSELECTED_OPACITY;
    return group;
}
function createNode(pt, id){
    var r = 50;
    var node = new Path.Circle(pt,r);
    node.fillColor = 'blue';
    var group=createImage(id,pt,node);
    arr.push(group);
    center.push(pt);
    selected.push(false);
}

function toggleHighlightController(event){
    for(var i=0;arr.length>i;i++){
        if(arr[i].contains(event.point)){
            toggleHighlight(i);
            return true;
        }
    }
    return false;

}
function highlight(i,color){
    var node=arr[i];
    var pt=center[i];
    if(path.contains(pt)){
        // var a=new Path.Circle(pt,node.length/Math.PI/2);
        selected[i]=true;
        nodeView(node,selected[i]);
        return true;
    }
    return false;
}


function nodeView(node,selected){
    selected?node.opacity=SELECTED_OPACITY:node.opacity=UNSELECTED_OPACITY;
}

function toggleHighlight(i){
    if(selected[i]){
        selected[i]=false;
        nodeView(arr[i],selected[i]);
    } else {
        selected[i]=true;
        nodeView(arr[i],selected[i]);
    }
}

function unselect(){
    for(var i=0;arr.length>i;i++){ //could just selected selected ones.
        selected[i]=false;
        nodeView(arr[i],selected[i]);
    }
}
function multipleSelect(){
    var notEmpty=false;
    for(var i=0;arr.length>i;i++){
        notEmpty=highlight(i,SELECTED_COLOR)||notEmpty;
    }
    return notEmpty;
}

var textItem = new PointText({
    content: 'Click and drag to draw a line.',
    point: new Point(20, 30),
    fillColor: 'black'
});

function onMouseDown(event) {
    // If we produced a path before, deselect it:
    if (path) {
        path.selected = false;
    }

    // Create a new path and set its stroke color to black:
    path = new Path({
        segments: [event.point],
        fillColor: 'black',
        opacity:.1
        // Select the path, so we can see its segment points:
        // fullySelected: true
    });
}

// While the user drags the mouse, points are added to the path
// at the position of the mouse:
function onMouseDrag(event) {
    path.add(event.point);
    multipleSelect();
    // Update the content of the text item to show how many
    // segments it has:
    textItem.content = 'Segment count: ' + path.segments.length;
}

// When the mouse is released, we simplify the path:
function onMouseUp(event) {
    var segmentCount = path.segments.length;

    // When the mouse is released, simplify it:
    path.simplify(10);

    // Select the path, so we can see its segments:
    // path.fullySelected = true;
    path.closePath();
    path.remove();

    //justselection notEmpty==false
    // if all selected are not alreadyselect or all are alreadyselect
    if(!multipleSelect())
        if(!toggleHighlightController(event))
            unselect();

    var newSegmentCount = path.segments.length;
    var difference = segmentCount - newSegmentCount;
    var percentage = 100 - Math.round(newSegmentCount / segmentCount * 100);
    textItem.content = difference + ' of the ' + segmentCount + ' segments were removed. Saving ' + percentage + '%';
}