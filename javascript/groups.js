
var firsttime;
var heights = [0];
if (window.location.hash && window.location.hash == '#_=_') {
	window.location = '';
}

function drawGroups(makeGroup) {
	if (typeof(makeGroup) == 'string')
		makeGroup = JSON.parse(makeGroup);
	if (!(makeGroup instanceof Array) && (makeGroup instanceof Object)) {
		makeGroup = makeGroup.group;
	}
	console.log('makeGroup[0]');
	var recommendations=$('.profiles').length;
	if(groups.length>recommendations){
		console.log(recommendations+'recommendations');
		drawGroup(groups[recommendations],recommendations+1);
	}
	profileListener();
	friendlistListeners();
}
function tabsListener(tab) {
	$('#' + tab + 'Button').on('click', function () {
		selectTab(tab);
		if (!$("#postingSection").hasClass("hidden")) {
			$("#postingSection").toggleClass("hidden");
		}
		if (!$("#friendlistSection").hasClass("hidden")) {
			$("#friendlistSection").toggleClass("hidden");
			$.get('viewFriendlists', function (res) {
				if (res.friendlists) {
					localStorage.setItem("friendlists", JSON.stringify(res.friendlists.data));
				} else {
				}
				drawPostFL(res.friendlists.data);
			});
		}
		$('#' + tab + 'Section').toggleClass("hidden");
	});
}

function drawGroup(arr, num) {
	if(arr.length<=1)
	return;
	var $group = document.createElement('div');
	$group.className = 'group';
	$('#friendlistSection').append($group);

	$($group).append('<input placeholder="Name of Group ' + num + '">');
	var $profiles = document.createElement('div');
	$profiles.className = 'profiles';
	$profiles.id = 'profiles' + (num);
	$($group).append($profiles);

	var name;
	//var i in arr
	for (var i=0; arr.length>i;i++) {
		if (fbObject[arr[i]]) {
			if (fbObject[arr[i]].first_name) {
				name = fbObject[arr[i]].first_name;
			} else if (fbObject[arr[i]].last_name) {
				name = fbObject[arr[i]].last_name;
			}
			$($profiles).append('<div class="profile" friend="' + arr[i] + '"><span>' + name + '</span><img src="' + fbObject[arr[i]].picture + '">');
		}
	}

	var $create_group = document.createElement('div');
	$create_group.className = 'create_group';
	$($group).append($create_group);

	$($create_group).append('<button class="btn">Create Group');
	$($group).append('<img id="loadingImage" src="http://upload.wikimedia.org/wikipedia/commons/d/de/Ajax-loader.gif">');

	heights.push(parseInt($($group).css('height')) + heights[heights.length - 1]);
}

function initialLoadingRemove(){
	$('#loadingImage').remove();
	$('#loadingMessage').remove();
}
var talk=0;
$(window).on('scroll', function () {

	clearTimeout($.data(this, 'timer'));
	$.data(this, 'timer', setTimeout(function() {
		profileListener();
		friendlistListeners();
		$('.group input').css('display','');
		$('.group:in-content:first input').css('display','block');
		$('.group button').css('display','');
		$('.group:in-content:first button').css('display','block');
	}, 50));
	if((window.innerHeight + $(window).scrollTop()+1000) >= $("body").height()){
		var recommendations=$('.profiles').length;
		if(groups.length>recommendations){
			$("#loadingImage").remove();
			drawGroup(groups[recommendations],recommendations+1);
		} else {
			$("#loadingImage").remove();
		}
	}


});

function profileListener(){
	$('.profiles:in-viewport').off('click','div:in-viewport').on('click', 'div:in-viewport',function () {
		$(this).toggleClass('inactive');
	});
	$('.profiles:in-viewport').off('click',' div:not(:in-viewport)');
}
function cacheImages(fbdata) {
	var imgarr = new Array();
	var j = 0;
	for (var i in fbdata) {
		imgarr[j] = new Image();
		imgarr[j].src = fbdata[i].picture;
	}
}

function selectTab(tab) {
	localStorage.setItem('lastSelected', tab);
	$('#' + tab + 'Button').parent('.tabrow').children('div').removeClass('selected');
	$('#' + tab + 'Button').toggleClass('selected');
	if (tab == 'friendlist') {
		$("#postingSection ").addClass("hidden");
		$("#friendlistSection ").removeClass("hidden");
		$(".friendlist").off('click','div');
		profileListener();

		// posting default
	} else if (tab == 'posting') {
		$("#friendlistSection ").addClass("hidden");
		$("#postingSection ").removeClass("hidden");
		postingFriendlistListener();
	}
}

function friendlistTabListener() {
	tabsListener("friendlist");
}

function reveal() {
	postingReveal();
	friendlistReveal();
	$('.tabrow').removeClass('initialhide');
}
function postingReveal() {
	$("#postingSection").removeClass("initialhide");
	$(".postfield").removeClass("initialhide");
}
function friendlistReveal() {
	$("#friendlistSection ").removeClass("initialhide");
	$("#groupBackground ").removeClass("initialhide");
}
function postingTabListener() {
	tabsListener("posting");
}
function friendlistListeners() {
	$('.group .btn:in-viewport').off("click").on("click", function () {
		var idsOfGroup = [];
		var rejected = [];
		var groupContainer = $(this).parentsUntil('.group').parent();
		groupContainer.find(" .profiles > *").each(function () {
			if (!$(this).hasClass("inactive")) {
				idsOfGroup.push($(this).attr("friend"));
			} else {
				rejected.push($(this).attr("friend"));
			}
		});

		var groupname = groupContainer.find("input").val();
		if (groupname) {
			if (!friendlists) {
				getFriendlist(function () {
					return postFriendList(groupContainer, {name: groupname, id: idsOfGroup, rejected: rejected, friendlists: friendlists, userid: userid});
				});
			} else {
				postFriendList(groupContainer, {name: groupname, id: idsOfGroup, rejected: rejected, friendlists: friendlists, userid: userid});
			}
			$groupContainer.find(".profiles").html("Post to"+groupname);
		} else {
			alert("Please enter group name");
		}
	});

	$('.group .btn:not(:in-viewport)').off("click");
}

function postFriendList(groupContainer, obj) {
	return $.post("friendlist", obj, function (data) {
		groupContainer.find(".create_group").toggleClass('friendlistMade');
		groupContainer.find(".create_group").html(data);
	});
}
function getFriendlist(callback) {
	FB.api('me?fields=friendlists', function (res) {
		console.log('getfriendlist');
		drawPostFL(res.friendlists.data);
		if (callback)
			callback();
	});
}
function postingListeners() {
	$('#postingSection button').on('click', function () {
		var idsOfGroup = [];
		var namesOfGroup = [];
		var rejected = [];
		var groupContainer = $('#postingSection');
		groupContainer.find(" .friendlist > *").each(function () {
			if ($(this).hasClass("selectedFriendList")) {
				idsOfGroup.push($(this).attr("friendlist"));
				namesOfGroup.push($(this).html());
			} else {
				rejected.push($(this).attr("friendlist"));
			}
		});
		var flStr = JSON.stringify(idsOfGroup).replace('[', '').replace(']', '').replace(/"/g, '');
		postToFacebook(flStr, namesOfGroup);
	});
	postingFriendlistListener();
}
function postingFriendlistListener(){
	$(".friendlist").off('click','div');
	$(".friendlist").on('click', 'div', function () {
		$(this).toggleClass("selectedFriendList");
	});
}

function drawPostFL(fl) {
	friendlists = fl;
	$(".friendlist").html('');
	var clearfix = '';
	if (fl && fl.paging)
		delete fl.paging;
	localStorage.setItem("friendlists", JSON.stringify(fl));
	for (var i in fl) {
		if (i == fl.length - 1) {
			var clearfix = "class='clearfix'";
		}
		$('.friendlist').append('<div friendlist="' + fl[i].id + '">' + fl[i].name + '</div>');
	}
}

function postToFacebook(str, namesOfGroup) {
	if (str == '') {
		$.post('posting', {message: $('#postingSection textarea').val(), privacy: {value: 'ALL_FRIENDS'}}, function (msg, r) {
			$("#postingSection").append("<div>" + msg + " to everyone</div>");
		});
	} else {
		$.post('posting', {message: $('#postingSection textarea').val(), privacy: {value: 'CUSTOM', allow: str}}, function (msg, r) {
			if (r == "success") {
				$(".confirmPost").html("Posted message to " + namesOfGroup.toString().replace(',', ', '));
			}
		});
	}
}

function loadbar(selector, loadingPeople) {
	$(document).ready(function () {
		$(selector).on('transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd', function () {
			$(selector).css('transition', '1s');
			$(selector).css('width', '100%');
		});
		var transitionTime = Math.ceil(loadingPeople / 1000 * 18 - 1) + 's';
		var width = (Math.ceil(loadingPeople / 1000 * 18 - 1) - 1) / Math.ceil(loadingPeople / 1000 * 18) * 100 + '%';
		$(selector).css('transition', transitionTime);
		$(selector).css('width', width);
	});
}
function unpack(a) {
	if (typeof a === 'string')
		return JSON.parse(a);
	else {
		return a;
	}
}

function getInfo(accessToken, userid) {
	$.get("UserHasLoggedIn", {accessToken: accessToken, userid: userid}, function (res) {
		if (res.empty) {
			$('#loadbar').removeClass('initialhide');
			$('#loading').append('<span id="loadtext">Getting ' + res.loadingPeople + ' friends and thier friends.</span>');
			loadbar('#loading', res.loadingPeople);
			$.get("formnew", {loadingPeople: res.loadingPeople, userid: res.userid}, function (res) {
				$('#loadbar').remove();
				draw(res);
			});
		} else {
			if(typeof res.fbObject=='string'){
				res.fbObject=JSON.parse(res.fbObject);
			}
			res.loadingPeople = Object.keys(res.fbObject).length;
			draw(res);
		}

	});
}

function draw(res) {
	groups = unpack(res.groups);
	localStorage.setItem("groups", JSON.stringify(res.groups));
	fbObject = unpack(res.fbObject);
	localStorage.setItem("fbObject", JSON.stringify(res.fbObject));
	drawGroups(groups);
}
function initialLoginScreen() {
	initialLoadingRemove();
	$('body').toggleClass('logo');
	var loginContainer = document.createElement('div');
	loginContainer.className = 'center_container';
	var loginCenter = document.createElement('div');
	loginCenter.className = 'center';

	var content = "<br><div class='title'>Privacy is Important.</div>" +
		"<br> That's why we are making it easier." +
		"<br> Intelligently organize your Facebook friends." +
		"<br> Manage your privacy better for posting and events.<br><br><br><br>" +
		"<a class='btn' href='/auth/facebook'>Connect</a>";
	$(loginCenter).append(content);
	$(loginContainer).append(loginCenter);
	$('body').prepend(loginContainer);
}

