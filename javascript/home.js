var fbObject;
var groups;
var friendlists;
var userid;

friendlistTabListener();
postingTabListener();

var fbObject = localStorage.getItem('fbObject');
var friendlists = localStorage.getItem('friendlists');
var groups = localStorage.getItem('groups');
var lastSelected = localStorage.getItem('lastSelected');

var time=[];

var base=Date.now();
window.fbAsyncInit = function() {
	FB.init({
		appId      : '1423201991259167',
		status     : true,
		version	   : 'v2.0',
		frictionlessRequests : true
	});

	time[0]=Date.now();
	FB.getLoginStatus(function(res){
		time[1]=Date.now();
		getFriendlist();
		if(res.status=="unknown"){
			initialLoginScreen();
		} else {
			if(fbObject&&friendlists&&groups){
				time[3]=Date.now();
				fbObject=JSON.parse(fbObject);
				friendlists=JSON.parse(friendlists);
				groups=JSON.parse(groups);
				time[4]=Date.now();
				$('#loadbar').remove();
				initialLoadingRemove();
				reveal();
				time[5]=Date.now();
				selectTab('posting');
				time[6]=Date.now();
				drawPostFL(friendlists);
				drawGroups(groups);
				time[7]=Date.now();
				friendlistListeners();
				postingListeners();
				time[8]=Date.now();
				if(lastSelected){
					selectTab(lastSelected);
				} else {
					selectTab('friendlist');
				}
			}
			if(res.status=== 'connected'){
				initialLoadingRemove();
				reveal();
				time[2]=Date.now();
				userid=res.authResponse.userID;
				var url=window.location;
				if(url.search.indexOf('firsttime')!=-1){
					selectTab('friendlist');
					$("#postingButton").on('click',function(){
						window.location='/auth/fbpost';
					});
				} else {
					if(!lastSelected){
						selectTab('friendlist');
					}
				}
				getInfo(res.authResponse.accessToken,userid);

			} else {
				initialLoginScreen();
			}
		}


		for(var k=0;time.length-1>k;k++) {
			console.log(k);
			console.log(time[k]-base);
		}
	});

};
(function(d){
	var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
	if (d.getElementById(id)) {return;}
	js = d.createElement('script'); js.id = id; js.async = true;
	js.src = "//connect.facebook.net/en_US/all.js";
	ref.parentNode.insertBefore(js, ref);
}(document));

function removeLocalStorage(){
	localStorage.removeItem('fbObject');
	localStorage.removeItem('friendlists');
	localStorage.removeItem('groups');
	localStorage.removeItem('lastSelected');
}