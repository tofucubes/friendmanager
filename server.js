// tag plus visualization underneath
// subtraction has image of
var agent = require('webkit-devtools-agent');

//setup Dependencies
var express = require('express')
, app = express()
, http = require('http')
, server = http.createServer(app)
, assert = require("assert")
, connect = require('connect')
, fs = require('fs')
, graph = require('fbgraph')
, socketio = require('socket.io')
, path = require('path')
, mongoose = require('mongoose')
, port = (process.env.PORT || 8081 || 80)
, serverconfig = require('./serverConfig')
, sharedJS = require('./javascript/sharedJS')
, zlib=require('zlib');

var Schema = mongoose.Schema;
//var addon = require('./javascript/build/Release/hello');
//console.log(addon.hello());
var continueSchema = new mongoose.Schema({
	userid: Number,
	loggedIn: Boolean,
	postedPermission: Boolean,
	lastPage:String
});
var userFBModel = new mongoose.Schema({
	userid: Number,
	fbObject: Array,
	friendlists: Array,
	groups:Array,
	rejects:Array,
	timestamp:Number
});
var analysisModel = new mongoose.Schema({
	userid: Number,
	friendlists: Array,
	groups:Array,
	rejected:Array,
	timestamp:Number
});
var friendlistsModel = new mongoose.Schema({
	userid: Number,
	friendlists: Array,
	timestamp:Number
});
var groupsModel = new mongoose.Schema({
	userid: Number,
	groups: Array
});

var Analysis = mongoose.model('Analysis', analysisModel);
var Friendlist = mongoose.model('Friendlist', friendlistsModel);
var User = mongoose.model('User', userFBModel);
var GroupsCollection = mongoose.model('Groups', groupsModel);
var Continue = mongoose.model('Continue', continueSchema);
var dbOff;
mongoose.connection.on('error', function (err) {
	console.log('Could not connect to mongo server!');
	dbOff = true;
	console.log(err);
});
mongoose.connection.on('open', function (ref) {
	dbOff = false;
	console.log('Connected to mongo server.');
});
mongoose.connect('mongodb://localhost/test');
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function callback () {
	console.log('database connected');
});



// CONSTANTS
var VERY_SMALL_GROUP = 5;
var SMALL_GROUP = 10;
var MEDIUM_GROUP = 15;
var BIG_GROUP = 25;
var CONNECTIVITY = new Array();
var FRIENDLIST='friendlistPage';
var FRIENDS_IN_BATCH = 200;
var POPULAR_PEOPLE = 50;

var opts;
app.use(express.logger());
var conf = {
    client_id: '1423201991259167', client_secret: 'bdc94facaceca535edef5bf0ac60fd78', scope: 'email, read_friendlists, manage_friendlists, user_friends', redirect_uri: serverconfig.redirect_uri
};
var authUrl = graph.getOauthUrl({
    "client_id": conf.client_id, "redirect_uri": conf.redirect_uri
});
var options = {
    timeout: 3000, pool: { maxSockets: Infinity }, headers: { connection: "keep-alive" }
};


// Setup Express
// Server Configuration
var server = express();
server.configure(function () {
    server.set('views', __dirname + '/views');
    server.set('view options', { layout: false });
    server.set('view engine', 'jade');
    server.use(connect.bodyParser());
    server.use(express.cookieParser());
    server.use(express.session({ secret: "shhhhhhhhh!"}));
    server.use(connect.static(__dirname + '/static'));
    server.use(server.router);
});
server.configure('development', function () {
    server.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
});
server.configure('production', function () {
    server.use(express.errorHandler());
});
//setup the errors
server.use(function (err, req, res, next) {
    if (err instanceof NotFound) {
        res.render('404.jade', {
            title: '404 - Not Found', description: '', author: '', analyticssiteid: 'XXXXXXX', status: 404 });
    } else {
        res.render('500.jade', {
            title: 'The Server Encountered an Error', description: '', author: '', analyticssiteid: 'XXXXXXX', status: 500 });
    }
});
server.listen(serverconfig.port);
clog('[%s] Server running at http://127.0.0.1:8080/', process.pid);


//Setup Socket.IO
var io = socketio.listen(server);
io.sockets.on('connection', function (socket) {
    clog('Client Connected');
    socket.on('message', function (data) {
        socket.broadcast.emit('server_message', data);
        socket.emit('server_message', data);
    });
    socket.on('disconnect', function () {
        clog('Client Disconnected.');
    });
});


///////////////////////////////////////////
//              Routes                   //
///////////////////////////////////////////

/////// ADD ALL YOUR ROUTES HERE  /////////

// server.get('/', function(req,res){
//   res.render('index.jade', {
//               title : 'Your Page Title'
//              ,description: 'Your Page Description'
//              ,author: 'Your Name'
//              ,analyticssiteid: 'XXXXXXX'
//   });
// });

server.get('/', function (req, res) {
	res.render("index", { title: "Connect" });
});
server.get('/fbtest', function (req, res) {
	res.render("fbtest");
});
server.get('/facebookautoload', function(req, res) {
	res.render("facebookautoload", {});
});

server.use("/javascript", express.static(__dirname + '/javascript'));
server.use("/css", express.static(__dirname + '/css'));

function authFB(req,res,post){
	if (!req.query.code){
		var scope=conf.scope;
		if(post){
			scope+=' publish_stream';
		}
		var authUrl = graph.getOauthUrl({
			"client_id": conf.client_id, "redirect_uri": conf.redirect_uri, "scope": scope
		});
		if (!req.query.error) { //checks whether a user denied the app facebook login/permissions
			console.log('not logged in');
			res.redirect(authUrl);
			console.log(authUrl);
			return "hi";
		} else {  //req.query.error == 'access_denied'1423201991259167
			res.send('access denied');
		}
	}

	// code is set
	// we'll send that and get the access token
	graph.authorize({
		"client_id": conf.client_id, "redirect_uri": conf.redirect_uri, "client_secret": conf.client_secret, "code": req.query.code
	}, function (err, facebookRes) {
		opts = {access_token: req.query.code};
		settings('loggedIn',true,function(){res.redirect('/')},
			function(){res.redirect('/?firsttime');
			}
			);
	});
}

function settings(setting,value,callback1,callback2){
	graph.get('me?fields=id',function (q,r){
		Continue.findOne({userid: r.id},setting,function (err, dbRes) {
			if(!dbRes){ //new entry
				var currentContinue = new Continue({userid:r.id, loggedIn:true,postedPermission:false,lastPage:FRIENDLIST});
				currentContinue.save(function (err, result){
					if (err) return console.error(err);
				});
				callback2();
			} else if(dbRes[setting]==value){
				callback1();
			} else {
				callback2();
			}
			if (err) return console.error(err);
		});
	});

}
server.get('/auth/facebook', function (req, res){
	// we don't have a code yet
	// so we'll redirect to the oauth dialog
	authFB(req,res);
});
server.get('/auth/fbpost', function (req, res){
	// we don't have a code yet
	// so we'll redirect to the oauth dialog
	var post=true;
	authFB(req,res,post);
});

server.post('/friendlist', function (req, serverres){
    var parameters = serverres.req.body;
    var FL = {name: parameters.name
    };
    var members = {
        members: parameters.id
    };
	// TODO: create updating delete by saving both existing and proposed list - rejects. delete list and recreate list
	console.log('parameters.friendlists');
	console.log(parameters.friendlists);
	var fl = parameters.friendlists.filter(function(a){return a.name==parameters.name});
	var poststring;
	var didWhat;

	//TODO: write to database rejected
	User.find(function (err, users) {
		if (err) return console.error(err);
		dlog(users)
	});

	if(!fl){
		didWhat="Created";
		graph.post("/me/friendlists", FL, function(err, resFriendlists) {
			console.log('resFriendlists.id');
			console.log(resFriendlists.id);
			console.log('name');
			console.log(FL);
			console.log('members');
			console.log(members);
			poststring="/"+resFriendlists.id+"/members";
			//parameters.userid

			addNewMembers(poststring,members,parameters.name,didWhat);
		});
	} else {
		poststring="/"+fl.id+"/members";
		didWhat="Updated";
		addNewMembers(poststring,members,parameters.name,didWhat)
	}
	function addNewMembers(url,members,name,didWhat){
		graph.post(url, members, function(err, resMembers) {
			if(resMembers!=null){
				serverres.end(didWhat+" "+name+" FriendList.");
			} else {
				serverres.end(name+" FriendList has added no new members");
			}
		});
	}
});

server.post('/posting', function(req,resPost){
	if(req.body.privacy.allow){
		console.log(req.body.privacy.allow);
		console.log(req.body.message);
		graph.post('me/feed',{message:req.body.message,privacy:{'value':'CUSTOM','allow':req.body.privacy.allow}},function(q,s){
			resPost.end('Posted');
		});
	} else {
		graph.post('me/feed',{message:req.body.message,privacy:{'value':'ALL_FRIENDS'}},function(q,s){
			resPost.end('Posted');
		});
	}
});
server.get('/viewFriendlists', function(req,resFI){
	if(getObject(resFI).accessToken)
		graph.setAccessToken(getObject(resFI).accessToken);
	graph.get('me?fields=friendlists',function(err,res){
		resFI.send(res);
	})
});


function friendinfo(userid,numberOfFriends,callback){
	var fbObject = new Object();
	var batchqueries = [batchget("me?fields=friendlists.fields(members,name)")];
	function friendCycles(n) {
		while (n--)
			batchqueries.push(batchget("me?fields=friends.offset(" + FRIENDS_IN_BATCH * (n) + ").limit(" + FRIENDS_IN_BATCH + ").fields(mutualfriends,picture.type(square).width(120).height(120),first_name,last_name)"));
	}
	friendCycles(Math.ceil(numberOfFriends/ FRIENDS_IN_BATCH));
	batchqueries = JSON.stringify(batchqueries);

	graph.post("?batch=" + batchqueries, function (req, res) {
		var friendlists = fbBatchExtract(res[0]);
		Analysis.findOne({userid: friendlists.id},'',function (err, dbRes) {
			if(dbRes!=null){
			} else {
				friendlists.friendlists=friendlists.friendlists.data;
				var currentAnalysis = new Analysis({userid:friendlists.id, friendlists:friendlists.friendlists.data, timestamp:Date.now()});
				currentAnalysis.save(function (err, currentAnalysis){
					if (err) return console.error(err);
					dlog('saved currentAnalysis');
				});
			}
		});
		res.shift();
		fbObject=facebookDataToHashtable(res);
		User.findOne({userid:userid},function (err, users) {
			if (err) return console.error(err);
			//	console.log('users'+users);
			if(users!=null){
				//	console.log('already created');
			} else {
				var currentUser = new User({userid:userid,fbObject:[fbObject],friendlists:friendlists.friendlists});
				currentUser.save(function (err, currentUser){
					if (err) return console.error(err);
				});
			}
		});
		var packet={fbObject:fbObject,friendlists:friendlists.friendlists};
		for(var p in packet){
			console.log(p);
			console.log(typeof(packet[p]));
		}
		if(callback){
			callback(packet);
			return;
		} else {
			return packet
		}
	});
}

server.get('/friendinfo', function(req,resFI){
	resFI.send(friendinfo(getObject(resFI).userid,getObject(resFI).numberOfFriends));
});

//rank and filter
server.get('/UserHasLoggedIn', function (req, serverres){
	console.time('fbrequest');
	var parameters=new Object();
	graph.setAccessToken(getObject(serverres).accessToken);

	graph.get("me", function (err, friendsCountAndID) {
		parameters.userid=friendsCountAndID.id;
		if(dbOff){
			console.log('turn mongod on');
		} else
			User.findOne({userid:friendsCountAndID.id},'fbObject friendlists',function (err, dbRes) {
				if (err) return console.error(err);
				if(dbRes){
					parameters.fbObject=dbRes._doc.fbObject[0];
					GroupsCollection.findOne({userid:friendsCountAndID.id},'groups',function(err,groupsarr){
						if(groupsarr){
							parameters.groups=groupsarr.groups;
							serverres.send(parameters);
						} else {
							console.log('no groups');
							serverres.send(parameters);
						}
					});
					console.log('indatabase');
				} else {
					graph.get("me/friends", function (err, friendsCountAndID) {
						console.log('doesnt exist');
						parameters.loadingPeople=friendsCountAndID.data.length;
						parameters.empty=true;
						serverres.send(parameters);
					});
				}
			});
    });
});

server.get('/formnew',function(req,serverres){
	var parameters={'userid':getObject(serverres).userid,'loadingPeople':getObject(serverres).loadingPeople};
	friendinfo(parameters.userid,parameters.loadingPeople,function(e){
		parameters=mergeObjects.apply(null,[parameters,e,makeGroups(e.fbObject,parameters.userid)]);
		serverres.send(parameters);
	});
});
//the right path

function clog(str){
//    console.log(str);
}
function tlog(str){
//    console.log(str);
}
function dlog(str){
//    console.log(str);
}

function facebookDataToHashtable(res) {
	var friends = combineFacebook(res);
	var arr=[];
	var fbObject=new Object();
	for (var i = 0; friends.length > i; i++) {
		var friendsHashtable = new Object();
		var total_mutual;
		var friend_id = friends[i].id;
		fbObject[friend_id] = new Object();
		if (friends[i].mutualfriends && friends[i].mutualfriends.data) {
			total_mutual = friends[i].mutualfriends.data;
			for (var mutual_i = 0; total_mutual.length > mutual_i; mutual_i++) {
				friendsHashtable[total_mutual[mutual_i].id] = true;
			}
			var totalOfEverything=0;
			fbObject[friend_id].total = total_mutual.length;
			if(total_mutual.length){
				totalOfEverything+=total_mutual.length;
				arr.push(total_mutual.length);
			}
		}
		fbObject[friend_id].mutual = new Object();
		fbObject[friend_id].mutual = friendsHashtable;
		fbObject[friend_id].first_name = friends[i].first_name;
		fbObject[friend_id].last_name = friends[i].last_name;
		fbObject[friend_id].picture = friends[i].picture.data.url;
	}
	return fbObject;
}

function getObject(res){
	return res.req.query;
}
function postObject(res){
	return res.req.body;
}
function combineFacebook(res) {
	var friends = mergeObjects.apply(null, fbBatchExtract(res)).friends.data;
	friends.sort(function (a, b) {
		if (!(b.mutualfriends)) {
			return -1;
		} else if (!(a.mutualfriends)) {
			return 1;
		}
		return b.mutualfriends.data.length - a.mutualfriends.data.length;
	}); //nlog(n)
	return friends;
}
function fbBatchExtract(obj) {
	var arr = [];
	if (obj instanceof Array) {
		for (var i = 0; i < obj.length; i++) {
			arr.push(JSON.parse(obj[i].body));
		}
		return arr;
	}
	return JSON.parse(obj.body);
}
function urlfilter(str) {
    return str.replace(/\&/g, '%26').replace(/\(/g, '%28').replace(/\)/g, '%29');
    //ADDED "" be careful
}

function batchget(url) {
    return {"method": "GET", "relative_url": urlfilter(url)};
}

function desc(a,b){
    return b-a;
}


function mergeObjects() {
	var object = arguments[0];
	for (var i = 0; arguments.length - 1 > i; i++)
		object = merge2Objects(object, arguments[i + 1]);
	return object;
	function merge2Objects(obj, obj1) {
		for (var key in obj) {
			if (obj instanceof Array) {
				obj = obj.concat(obj1);
				return obj;
			}
			if (obj[key] instanceof Object && !(obj instanceof Array)) {
				if(obj1.hasOwnProperty(key)&&obj.hasOwnProperty(key)){
					obj[key] = merge2Objects(obj[key], obj1[key]);
				}
			}
		}
		for(var key1 in obj1){
			if(obj1.hasOwnProperty(key1)&&!obj.hasOwnProperty(key1)){
				 obj[key1]=obj1[key1];
			}
		}
		return obj;
	}
}

function makeGroups(fbObject,userid,groupblock){
	var alter=1;
	var groupblock=groupblock;
	if(typeof(groupblock)=='undefined');
	groupblock=new Array();
	var number_of_friends = Object.keys(fbObject).length;
	var grouparr = new Array();
	var notUnique = new Array();
	var totalgroups = new Array();

	var toolittle = 0;
	if(POPULAR_PEOPLE){} else POPULAR_PEOPLE=50;
	while (number_of_friends * .9 > groupblock.length && number_of_friends > groupblock.length+POPULAR_PEOPLE+1) {
		var anothergroup = makeAnotherGroup(groupblock);
		if(anothergroup.length<3){
			toolittle++;
		}
		if(toolittle>2){
			break;
		}
		var totalgroup = getAllTheRest(groupblock,getTotal);
		totalgroup.sort(desc);
		totalgroups.push(totalgroup);
		groupblock = groupblock.concat(complementArray(groupblock, anothergroup));
		var similarity = mostSimilarGroup(anothergroup, grouparr);
		if (similarity.similar > .3) {
			var group = grouparr[similarity.index].concat(complementArray(grouparr[similarity.index], anothergroup));
			var percent=complementArray(grouparr[similarity.index], anothergroup).length/anothergroup.length;
			notUnique.push({group:anothergroup,index:similarity.index,uniquepercent:percent});
		} else {
			if(anothergroup.length>2)
				grouparr.push(anothergroup);
		}
	}
	var groupEntry=new GroupsCollection({userid:userid,groups:grouparr});
	groupEntry.save(function (err, groupSave){
		if (err) return console.error(err);
		//dlog('groupSave');
		//dlog(groupSave);
	});

	return {groups:grouparr,similar:notUnique};
	function getTotal(id){
		return fbObject[id].total;
	}

	function getName(id){
		return fbObject[id].first_name+" "+fbObject[id].last_name;
	}
	function getFriends(id){
		return fbObject[id].mutual;
	}

	function isFriend(id1, id2){
		return fbObject[id1].mutual[id2];
	}

	function getAllTheRest(groupblock,fxn){
		var arr=[];
		for (var key in fbObject){
			if (!inGroup(key, groupblock)) {
				arr.push(fxn(key))
			}
		}
		return arr;
	}
	function getNewPeople(groupblock){
		var newpeople=getAllTheRest([],getManyInfo);
		newpeople.sort(function (a, b) {
			return b.total - a.total
		});
		groupForFinding2People = new Array();
		var testpeople=newpeople.length>POPULAR_PEOPLE?POPULAR_PEOPLE:newpeople.length;
		for (var key = 0; testpeople > key; key++) {
			groupForFinding2People.push(newpeople[key].id);
		}
	}


	function makeAnotherGroup(groupblock) {
		var newpeople=getAllTheRest(groupblock,getManyInfo);
		newpeople.sort(function (a, b) {
			return b.total - a.total
		});
		groupForFinding2People = new Array();

		var testpeople=newpeople.length>POPULAR_PEOPLE?POPULAR_PEOPLE:newpeople.length;
		for (var key = 0; testpeople > key; key++) {
			groupForFinding2People.push(newpeople[key].id);
		}
		var madeGroup=makeGroup(groupForFinding2People);
		if(madeGroup.length<5){
			var totals=0;
			var counter=0;
			for(var key in newpeople){
				totals+=newpeople[key].total;
				counter++;
				if(totals>250)
					break;
			}
			for (var key = POPULAR_PEOPLE+1; counter > key; key++) {
				groupForFinding2People.push(newpeople[key].id);
			}
			madeGroup=makeGroup(groupForFinding2People);
		}
		return madeGroup;
	}

	function makeGroup(finalgroup){
		likeness = findLikeness(finalgroup);
		mutualFriendsOfMostPopular = compareFriends(likeness[0][0], likeness[0][1]);
		var group = trimStddev(mutualFriendsOfMostPopular);
		if(CONNECTIVITY[fbObject['userid']]){
			alter=CONNECTIVITY[fbObject['userid']];
		} else if(alter==1){
			alter=3.5/qualityMetric(group);
			CONNECTIVITY[fbObject['userid']]=alter;
		}
		//TODO: fix when too small
		group.unshift(likeness[0][1]);
		group.unshift(likeness[0][0]);
		return createFriendGroup(group);
	}

	function createFriendGroup(group) {
		var metric = qualityMetric(group);
		if (metric < 2) {
			clog(metric);
			return group;
			//return finishgroup(group1);
		} else {
			clog(metric);
			//be aggressive as possible look for result above 2
			var progress = group.length;
			clog(progress);
			if (metric > 4 || group.length < VERY_SMALL_GROUP){
				group = verysmallgroup(group);
			}
			else if (metric > 3 || group.length < SMALL_GROUP){
				group = smallgroup(group);
			}
			if (metric > 2.3 || group.length < MEDIUM_GROUP)
				group = mediumgroup(group);
			if (metric > 2 || group.length < BIG_GROUP)
				group = biggroup(group);
			if (progress == group.length) {
				return finishgroup(group);
			}
			return createFriendGroup(group);
		}
	}

	function finishgroup(group1) {
		clog('finish');
		return xGroup(group1, 0,.8*Math.sqrt(1/alter));
	}
	function verysmallgroup(group1) {
		clog('very small');
		var results=VERY_SMALL_GROUP;
		if(alter>1)
			results=VERY_SMALL_GROUP/alter;
		return xGroup(group1, results,.2*Math.sqrt(1/alter));
	}
	function smallgroup(group1) {
		clog('small');
		var results=SMALL_GROUP;
		if(alter>1)
			results=SMALL_GROUP/alter;
		return xGroup(group1, results,.4*Math.sqrt(1/alter));
	}
	function biggroup(group1) {
		clog('big');
		var results=BIG_GROUP;
		if(alter>1)
			results=BIG_GROUP/alter;
		return xGroup(group1, results,.8*Math.sqrt(1/alter));
	}

	function mediumgroup(group1) {
		clog('medium');
		var results=MEDIUM_GROUP;
		if(alter>1)
			results=MEDIUM_GROUP/alter;
		return xGroup(group1, results, Math.sqrt(1/7.7) *.1);

	}
	function qualityMetric(group){
		return quality(group)* alter * Math.log(group.length);
	}
	//TODO: TDD quality
	//returns number
	function quality(seed) { //only to be used in development
		var groupMutual = mutualfriendsInGroup(seed);
		for (var i = 1; i < groupMutual.length; i++) {
			groupMutual[i].rank += groupMutual[i - 1].rank;
		}
		return groupMutual[seed.length - 1].rank / seed.length / seed.length;
	}

	// LIKENESS START

	//[id1,id2,connections]
	function findLikenessInitial() {
		var a = [];
		var i = 0;
		for (var key in fbObject) {
			i++;
			a.push(key);
			if (i == POPULAR_PEOPLE)
				break;
		}
		return findLikeness(a);
	}
	function findLikeness(group) { // O(n^2)
		var likeness = new Array();
		for (var i = 0; group.length > i; i++)
			for (var j = i + 1; group.length > j; j++) {
				likeness.push([group[i], group[j], compareFriends(group[i], group[j])]);
			}
		likeness.sort(function (a, b) {
			return b[2].length - a[2].length
		});
		return likeness;
	}
	//{id1:true,id2:...}
	function compareFriends(a, b) {
		var a_ = getFriends(a);
		var b_ = getFriends(b);
		if (getTotal(a) > getTotal(b))
			return ht2array(compareHT(b_, a_));
		else
			return ht2array(compareHT(a_, b_));
	}

	function ht2array(ht) {
		var arr = new Array();
		for (var key in ht) {
			arr.push(key);
		}
		return arr;
	}



	function getManyInfo(key){
		var total=0;
		if(getTotal(key)){
			total=getTotal(key);
		}
		return {name: getName(key), id: key, total: total};
	}

	//returns true false
	function inGroup(person, group) {
		for (var j = 0; group.length > j; j++)
			if (group[j] == person)
				return true;
		return false;
	}
	function compareHT(a, b) {
		var ht = new Object();
		for (var keys in a)
			if (a[keys] == b[keys])
				ht[keys] = true;
		return ht;
	}
	function complementArray(a, b) {
		var arr = new Array();
		for (var index in b) {
			if (a.indexOf(b[index]) == -1) {
				arr.push(b[index]);
			}
		}
		//all unique in b returned
		return arr;
	}
	// LIKENESS END


	function trimStddev(group) { //not really trimPercent just .68% too lazy you know
		return trim(group, .68);
	}
	//[id1,id2,..]
	function trim(groupMutual, trimby) {
		if (trimby > 1) {
			return trimN(groupMutual, trimby);
		} else if (trimby < 1) {
			return trimPercent(groupMutual, trimby);
		} else if (!trimby) {
			return trimPercent(groupMutual, .68);
		}
	}
	//[id1,id2...]
	function trimPercent(group, percent) { //O(n)
		if(!group||group.length==0){
			return [];
		}
		var groupMutual = mutualfriendsInGroup(group);
		var group1 = [];
		var group2 = [];
		var total = 0;
		for (var i = 1; i < groupMutual.length; i++) {
			groupMutual[i].rank += groupMutual[i - 1].rank;
		}
		var stddevTotal = (groupMutual[groupMutual.length - 1].rank * percent);

		for (var i = groupMutual.length - 1; 0 < i; i--) {
			if (stddevTotal > groupMutual[i].rank) {
				group1.push(groupMutual[i].id);
			} else {
				group2.push(groupMutual[i].id);
			}
		}
		return group1;
	}
	//[id1,id2...]
	function trimN(groupMutual, N) { //O(n)
		var groupMutual = mutualfriendsInGroup(group);
		var group1 = [];
		var group2 = [];
		for (var i = groupMutual.length; 0 < i; i--) {
			if (i > groupMutual.length - N) {
				group2.push(groupMutual[i].id);
			} else {
				group1.push(groupMutual[i].id);
			}
		}
		return group1;
	}
	//{id,rank}
	function mutualfriendsInGroup(seed) {
		return XInGroup(seed, function (a) {
			return a;
		});
	}

	function shelteredInGroup(seed) {
		return XInGroup(seed, function (groupConnection, id) {
			return groupConnection / getTotal(id)
		});
	}

	function XInGroup(seed, fxn) { //O(n^2)
		var integration = 0;
		var group = [];
		var k;
		for (var i = 0; seed.length > i; i++) {
			k = 0;
			for (var j = 0; seed.length > j; j++)
				if (i != j && getFriends(seed[i]) && isFriend(seed[i], seed[j])) {
					k++;
				}
			group.push({id: seed[i], rank: fxn(k, seed[i])});
		}
		group.sort(function (a, b) {
			return b.rank - a.rank
		});

		return group;
	}



	function log(logstr) {
		clog(logstr);
		clog(eval(logstr));
	}

	function name(arridstr) {
		clog(arridstr);
		var argumentsWithArr = Array.prototype.slice.call(arguments, 1);
		argumentsWithArr.unshift(eval(arridstr));
		clog(nameVal.apply(null, argumentsWithArr));
	}






	//TODO:guard against to bounds for result
	function xGroup(group1, result, percent) {
		connectionGroup = outsideConnected(group1);
		cgroup = ht2SortedArray(connectionGroup, valueClosure);

		newAddition = new Array();
		for (var index in cgroup)
			if (cgroup[index].result > result && cgroup[index].result / getTotal(cgroup[index].id) > percent) {
				newAddition.push(cgroup[index].id);
			}
		return group1.concat(newAddition);
	}
	function nameVal(arrid) {
		if (!arrid) {
			clog('warning:empty []');
		}
		if (arguments.length == 1 && (arrid instanceof Array)) {
			return applyFunctionsOnArrays(arrid, getName);
		} else if (arrid instanceof Array) {
			var argumentsWithArr = Array.prototype.slice.call(arguments, 1);
			argumentsWithArr.push(getName);
			argumentsWithArr.unshift(arrid);
			return applyFunctionsOnArrays.apply(null, argumentsWithArr);
		} else if (arrid instanceof Number) {
			return getName(arrid);
		}
	}

	function stringify(json) {
		clog(JSON.stringify(json));
	}

	function test() {
		var likeness = findLikenessInitial();
		if (likeness[0][0] == 411871 && likeness[0][1] == 422937) {
			clog('likeness success');
		} else {
			clog('likeness failed');
		}

		if (maxKey({'dfsdsf': 231, '2343432': 34}) == 'dfsdsf') {
			clog('maxKey success');
		} else {
			clog('maxKey failed');
		}

		function check(json) {
			clog(JSON.stringify(json));
		}
	}


	function mostX(arr, rank, filter) {
		if (!filter) {
			filter = 1;
		}
		var closed = 0;
		var max_key;
		for (var key in arr) {
			if (rank(arr, key) >= closed && filter(arr, key)) {
				closed = rank(arr, key);
				max_key = key;
			}
		}
		return max_key;
	}


	function shelteredProspects(arr) {
		if (arr instanceof Object)
			return ht2SortedArray(arr, shelteredClosure);
	}
	function ht2SortedArray(arr, fxn) {
		var finalArray = new Array();
		for (var key in arr) {
			finalArray.push({id: key, result: fxn(arr, key)});
		}
		finalArray.sort(function (a, b) {
			return b.result - a.result;
		})
		return finalArray;
	}
	// true false
	function shelteredFilter(percent) {
		return function (arr, key) {
			return shelteredClosure(arr, key) > percent;
		}
	}

	function valueShelteredFilter(value, shelter) {
		return function (arr, key) {
			return valueClosure(arr, key) > value && shelteredClosure(arr, key) > shelter;
		}
	}
	// number
	function valueShelteredClosure(arr, key) {
		return arr[key] * arr[key] / getTotal(key);
	}
	function shelteredClosure(arr, key) {
		return arr[key] / getTotal(key);
	}

	// true false
	function valueFilter(above) {
		return function (arr, key) {
			return arr[key] > above;
		}
	}
	// number
	function valueClosure(arr, key) {
		return arr[key];
	}


	function maxKey(obj) {
		if (obj instanceof Object) {
			return findMaxObjectKey(obj);
		} else if (obj instanceof Array) {
			return findMaxArrayKey(obj);
		}
		return;
		function findMaxObjectKey(arr) {
			var max = 0;
			var max_key;
			for (var key in arr)
				if (arr[key] >= max) {
					max = arr[key];
					max_key = key;
				}
			return max_key;
		}

		function findMaxArrayKey(array) {
			var max = Math.max.apply(Math, array);
			return array.indexOf(max);
		}
	}

	function arrayOfObjectsMaxValue(arr, key) {
		arr.sort(function (a, b) {
			return b[key] - a[key]
		});
		return arr[0];
	}


	function containsKey(arr, whatis) {
		for (var arraykey in arr)
			if (whatis == arraykey)
				return true;
		return false;
	}

	function applyFunctionsOnArrays(arr, keys_example, fxn_example) {
		var array = new Array();
		if (arguments.length > 2) {
			//list array, then all keys and function to apply to keys, then next keys and then fxn to apply to those keys and so on and so forth
			var arrayobj = arr[0];
			var keyarr = [];
			var functionarr = [];
			for (var whatis in arguments) {
				if (whatis != 0)
					if (typeof(arguments[whatis]) == "function") {
						var fxn = arguments[whatis];
						for (var key in keyarr) {
							functionarr[keyarr[key]] = fxn;
						}
						keyarr = [];
					} else if (containsKey(arrayobj, arguments[whatis])) {
						keyarr.push(arguments[whatis]);
					}
			}

			for (var key in arr) {
				for (var key2 in arr[key]) {
					if (functionarr[key2]) {
						array[key] = new Object();
						array[key][key2] = functionarr[key2](arr[key][key2]);
					} else {
						array[key][key2] = arr[key][key2];
					}
				}
			}
			return array;
		}

		for (var key in arr) {
			array[key] = arguments[1](arr[key]);
		}
		return array;
	}






	function NotFound(msg) {
		this.name = 'NotFound';
		Error.call(this, msg);
		Error.captureStackTrace(this, arguments.callee);
	}








	function makeLikenessGroup(likeness) {
		var UNIQUE_FRIEND_WHORES = 5;
		var i = 0;
		var arr = [];
		var finalGroup = [];
		while (UNIQUE_FRIEND_WHORES > 0 && i < likeness.length - 1) {
			if (inGroup(likeness[i][0], arr)) {
				if (!inGroup(likeness[i][1], finalGroup)) {
					finalGroup.push(likeness[i][1]);
					if (!inGroup(likeness[i][1], arr))
						arr.push(likeness[i][1]); // keep the flow going NOT REALLY 5 anymore it's okay
				}
			} else {
				UNIQUE_FRIEND_WHORES--;
				arr.push(likeness[i][0]);
				finalGroup.push(likeness[i][0]);
			}
			i++;
		}
		return finalGroup;
	}
	function pass(initialPeople, morePeople, knownPeople, percentKnown, i) {
		var newPeople = findNongroupPeople(initialPeople, morePeople);
		var finalPeople = addToCore(initialPeople, newPeople, knownPeople, percentKnown);
		if (i == 0 || i == undefined) {
			return finalPeople;
		} else {
			i--;
			return pass(finalPeople, morePeople, knownPeople, percentKnown, i);
		}
	}

	function findNongroupPeople(group, people) { //n=people
		var popular = [];
		var manyPeople = 0;
		for (i = 0; fbArray.length - 1 > i && manyPeople < people; i++) {
			if (!inGroup(i, group)) {
				manyPeople++;
				popular.push(i);
			}
		}
		return popular;
	}
	function addToCore(core, outsiders, groupFriends, percent) { //O(n*m) n=outsider, m=groupOfFriends
		var approved = [];
		var rejected = [];
		for (var i = 0; i < outsiders.length; i++) {
			var a = belongToGroup(outsiders[i], core); //O(m)

			if (a.total > groupFriends || a.percentOfGroup > percent) {
				approved.push(outsiders[i]);
			} else {
				rejected.push(outsiders[i]);
			}
		}
		if (approved != []) {
			core = core.concat(approved);
		}
		return core;
	}




	// [id,totals]
	function sheltered(seed, fxn) { //O(n^2)
		var arguments;
		var integration = 0;
		var group = [];
		var k;
		for (var i = 0; seed.length > i; i++) {
			k = 0;
			for (var j = 0; seed.length > j; j++)
				if (i != j && isFriend(seed[i], seed[j])) {
					k++;
				}
			var arr = new Array();
			arr = [seed[i]];
			for (var l = 1; arguments.length > l; l++)
				arr.push(arguments[l](seed, k, i));
			group.push(arr);
		}
		group.sort(function (a, b) {
			return a[1] - b[1]
		});
		return group;
	}
	function percentOfFriends(seed, k, i) {
		return k / (fbArray[i].total);
	}
	function percentOfGroup(seed, k, i) {
		return k / seed.length;
	}



	//{id1:connections,id2:...}
	function outsideConnected(group, mostConnected) { // O(n^2)
		if (!(mostConnected)) {
			mostConnected = new Object(); //TODO: I'll need to address this eventually
		}
		for (var i = 0; group.length > i; i++) {
			mostConnected = addToConnected({friend: group[i], allConnections: mostConnected, excludegroup: group});
		}
		return mostConnected;
	}
	//{id1:connections,id2:...}
	function addToConnected(data) {
		var mutualFriends = getFriends(data.friend);
		//TODO: fix connections the number for Felix is off.
		var mostConnected = data.allConnections;
		delete mostConnected[data.friend];
		var group = data.excludegroup;
		for (var friend in mutualFriends) {
			if (!inGroup(friend, group)) {
				if (mostConnected[friend]) {
					mostConnected[friend]++;
				} else {
					mostConnected[friend] = 1;
				}
			}
		}
		return mostConnected;
	}



	//returns mutual friends


	function array2ht(arr, prop) {
		var ht = new Object();
		for (var i = 0; arr.length > i; i++) {
			if (prop) ht[arr[i][prop]] = true;
			else ht[arr[i]] = true;
		}
		return ht;
	}

	function mergeArray(a, b) {
		for (var index in b) {
			if (a.indexOf(b[index]) == -1) {
				a.push(b[index]);
			}
		}
		//all unique in b
		return a;
	}

	function mostSimilarGroup(group, arr) {
		var findmax = 0;
		var indexMax = -1;
		var a;
		for (var index in arr) {
			a = sharedJS.compareArray(arr[index], group);
			if (findmax < a.length / group.length) {
				findmax = a.length / group.length;
				indexMax = index;
			}
		}
		return {index: indexMax, similar: findmax};
	}

	function percentOfIntersection(a, b) {
		var intersection = sharedJS.compareArray(a, b);
		if (a.length > b.length)
			return intersection.length / b.length;
		else
			return intersection.length / a.length;
	}



	//returns {total:,percentOfGroup:,percentOfFriends:}
	function belongToGroup(person, group) { //n=group
		var p = 0;
		for (var j = 0; group.length > j; j++)
			if (isFriend(person, group[j]))
				p++;
		return {total: p, percentOfGroup: p / group.length, percentOfFriends: p / getTotal(person)};
	}
}
server.post('/makeGroup',function(req,res){
	res.send(makeGroups(getObject(res).fbObject,getObject(res).groupblock));
});








//A Route for Creating a 500 Error (Useful to keep around)
server.get('/500', function (req, res) {
    throw new Error('This is a 500 Error');
});





////The 404 Route (ALWAYS Keep this as the last route)
//server.get('/*', function(req, res){
//    throw new NotFound;
//});

clog('Listening on http://0.0.0.0:' + port);


//        div(id="group"+j).
//        script(type="text/javascript").
//            google.setOnLoadCallback(drawVisualization([!{totals[j]}]),'group!{j}');
//		each friend in friends
//			img(src=friend.picture.data.url,id=friend.id,style="display:none")

// UX
// create a space for the parent friend blob
// child blobs should take roughly 80% of space of parent blob
// parent blob shouldn't react until a bit and it should enlarge
// and lightly region to be dropped in
// if too many child elements it shrinks and there's a hoverover that enlarges and lets you place them.

// top group bar or people
// hitting title let's you see users inside and pushes people out
// button appears that says change title on title let's you change title

// circling outside of group let you group together groups
// move boundaries take people out.

// on iphone or smaller
// "swap bar" groups push left and right for more groups and search for people
// if select 1 person it pops up with icon that says "related people" and "highlight more"
// highlight selected people with highlight icon


//TODO: banner percent and then expand with many images monocolor or groups